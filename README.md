# Recipe App API PRoxy


## Usage

### Environment Variables

* `LISTEN_PORT` - The port to listen on (default `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (Default: `9000`)


